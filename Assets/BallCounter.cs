﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallCounter : MonoBehaviour
{
    public static BallCounter count = null;

    [SerializeField]
    GameObject video;

    int value_ = 0;
    public int Value
    {
        get => value_;
        set {
            value_ = value;
            text.text = $"Nombre de balles : {value}";
            if (value == 3)
            {
                video.SetActive(false);
            }
        }
    }

    Text text;

    // Start is called before the first frame update  
    void Awake()
    {
        if (count == null)
        {
            count = this;
            text = GetComponent<Text>();
        } else {
            Destroy(this);
        }
    }

}
