﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity.Interaction;

public class BallSpawner : MonoBehaviour
{

    Leap.Controller controller;
    float level = 0;
    bool hasInstanciated = false;

    public Transform handTransform;

    public Vector3 instanciateOffset;

    public GameObject ballPrefab;

    GameObject instanciatedBall = null;

    GameObject grabedBall = null;
    // Start is called before the first frame update
    void Start()
    {
        controller = new Controller();
    }

    // Update is called once per frame
    void Update()
    {
        Frame frame = controller.Frame();
        if (frame.Hands.Count > 0)
        {
            Hand hand = GetLeftHand(frame.Hands);
            if (hand != null)
            {
                if (IsPalmUp(hand.PalmNormal))
                {
                    level = Mathf.Min(level + 0.1f, 1);
                }
                else
                {
                    level = Mathf.Max(level - 0.1f, 0);
                }
            }
        }
        if (level == 0)
        {
            hasInstanciated = false;
            if (instanciatedBall != null)
            {
                Destroy(instanciatedBall);
                instanciatedBall = null;
            }
        }
        if ((level == 1 && !hasInstanciated))
        {
            hasInstanciated = true;
            Vector3 offset = handTransform.right * instanciateOffset.x + handTransform.up * instanciateOffset.y + handTransform.forward * instanciateOffset.z;
            instanciatedBall = Instantiate(ballPrefab, handTransform.position + offset, transform.rotation);
            ParticleSystem particleSystem = instanciatedBall.GetComponent<ParticleSystem>();
            if (particleSystem != null)
            {
                particleSystem.Play();
            }
            Rigidbody rbody = instanciatedBall.GetComponent<Rigidbody>();
            instanciatedBall.transform.SetParent(handTransform);
            InteractionBehaviour interactionBehaviour = instanciatedBall.GetComponent<InteractionBehaviour>();
            if (interactionBehaviour != null)
            {
                interactionBehaviour.OnGraspBegin += OnGrab;
            }
        }
    }

    public void OnGrab()
    {
        if (BallCounter.count != null)
        {
            BallCounter.count.Value++;
        }
        InteractionBehaviour interactionBehaviour = instanciatedBall.GetComponent<InteractionBehaviour>();
        interactionBehaviour.OnGraspBegin -= OnGrab;
        instanciatedBall.transform.SetParent(null);
        Rigidbody rbody = instanciatedBall.GetComponent<Rigidbody>();
        rbody.isKinematic = false;
        grabedBall = instanciatedBall;
        interactionBehaviour.OnGraspEnd += OnGrabEnd;
        instanciatedBall = null;
    }

    public void OnGrabEnd()
    {
        if (grabedBall != null)
        {
            InteractionBehaviour interactionBehaviour = grabedBall.GetComponent<InteractionBehaviour>();
            interactionBehaviour.OnGraspEnd -= OnGrabEnd;
            Rigidbody rbody = grabedBall.GetComponent<Rigidbody>();
            rbody.isKinematic = false;
            grabedBall = null;
        }
    }

    Hand GetLeftHand(List<Hand> hands)
    {
        foreach (var hand in hands)
        {
            if (hand.IsLeft)
            {
                return hand;
            }
        }
        return null;
    }

    bool IsPalmUp(Vector palmNormal) => palmNormal.Dot(Vector.Up) < 0;
    
}
