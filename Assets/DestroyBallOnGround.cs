﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBallOnGround : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        GameObject ballObject = collision.gameObject;
        if (ballObject.tag == "Ball")
        {
            Destroy(ballObject);
        }
    }
}
