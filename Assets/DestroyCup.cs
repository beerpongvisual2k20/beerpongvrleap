﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCup : MonoBehaviour
{
    bool explosion = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Ball" && !explosion)
        {
            explosion = true;
            Destroy(other.gameObject);
            StartCoroutine(Explode());
        }
    }

    IEnumerator Explode()
    {
        if (TextScore.score != null)
        {
            TextScore.score.Value++;
        }
        ParticleSystem particleSystem = GetComponent<ParticleSystem>();
        if (particleSystem != null)
        {
            particleSystem.Play();
            foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
            {
                if (!(renderer is ParticleSystemRenderer))
                {
                    renderer.enabled = false;
                }
            }
            yield return new WaitForSeconds(particleSystem.main.duration);
            Destroy(this.gameObject);
        }
    }
}
