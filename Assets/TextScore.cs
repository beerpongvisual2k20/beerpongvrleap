﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScore : MonoBehaviour
{

    public static TextScore score = null;

    int value_ = 0;
    public int Value
    {
        get => value_;
        set {
            value_ = value;
            text.text = $"Score : {value}";
            if (value == 1) {
                winText.text = $"Bravo... Plus que {scoreObjective - 1}";
            }
            if (value == 2) {
                winText.text = "Prenez une balle et tentez de la jeter dans un des gobelets";
            }
            if (value == scoreObjective)
            {
                winText.text = "Bravo tu es vraiment très fort.";
                arrow.SetActive(false);
            }
        }
    }

    Text text;

    [SerializeField]
    Text winText;

    [SerializeField]
    GameObject arrow;

    [SerializeField]
    int scoreObjective = 10;

    // Start is called before the first frame update
    void Awake()
    {
        if (score == null)
        {
            score = this;
            text = GetComponent<Text>();
        } else {
            Destroy(this);
        }
    }
}
